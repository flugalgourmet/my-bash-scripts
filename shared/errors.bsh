#!/bin/bash

declare -A PARAMETER_ARRAY
PARAMETER_INDEX=0

## Custom errors
EX_UNKNOWN=1

## Outputs warning messages and colors the output red if it's an interactive terminal. @param $1...: Messages
function sharedErrorsOutputMessage {
	local color=$1
	local error_type=$2
	local source=$3
	local function=$4
	local line_number=$5
	local message=$6
	local exit_code=$7
	## COLORS
	# 4=red
	test -t 1 && tput setf $color
	local colon=:
	local space=" "
	local date=$(date +%A,\ %B\ %e\ %Y)
	local time=$(date +%H:%M:%S)
	local uppercase_error_type=$(sharedStringsConvertToUppercase "$error_type")
	local text="$uppercase_error_type$colon$spaceOn $date at the time of $time an $error_type of '$message' was thrown in the file '$source' under the function '$function' on the line number of '$line_number' with an exit code of '$exit_code'."
	local index_length=${#INDEX_ARRAY[@]}
	for (( a = 0 ; a < $index_length ; a++ )); do
		local c=1
		local list=(""${INDEX_ARRAY[$a]}"")
		local number_of_parameters=${#list[@]} 
		declare -a parameter_array
		for (( b = 0 ; b < $number_of_parameters ; b++ )); do
			if sharedNumbersIsEqualTo $b 0; then
				local file_name=${list[$b]}
			elif sharedNumbersIsEqualTo $b 1; then
				local function_name=${list[$b]}
			else
				parameter_array=("${parameter_array[@]}" ${list[$b]})
			fi
		done
		text="$text\n\tBACKTRACE $a:\n\t\tFILE NAME: $file_name\n\t\tFUNCTION NAME: $function_name"
		for parameter in "${parameter_array[@]}"; do
			text="$text\n\t\tPARAMETER $c: $parameter"
			let "c += 1"
		done
		unset parameter_array
	done
	unset INDEX_ARRAY
	PARAMETER_INDEX=0
	sharedLogOutputEntry "$uppercase_error_type" "$source" "$function" "$line_number" "$text"
	test -t 1 && tput sgr0 # Reset terminal
	true
}

## Outputs error messages with optional exit code. Output set to the terminal is colored red. Output sent to a log is prefixed with 'ERROR: '
## USAGE: sharedErrorsThrowError "$BASH_SOURCE" "$FUNCNAME" $LINENO "$message" [$exit_code]
function sharedErrorsThrowError {
	if sharedVariablesIsSet "$1" && sharedVariablesIsSet "$2" && sharedVariablesIsSet "$3" && sharedVariablesIsSet "$4"; then
		local source=$1
		local function=$2
		local line_number=$3
		local message=$4
		if sharedVariablesIsSet $5; then
			local exit_code=$5
		else
			local exit_code=1
		fi
		sharedErrorsOutputMessage 4 error "$source" $function $line_number "$message" $exit_code
	elif sharedStringsIsNotSet "$1"; then
		sharedErrorsThrowError "$BASH_SOURCE" "$FUNCNAME" $LINENO "The internal 'BASH_SOURCE' parameter has not been passed to this function! The error message cannot be compiled without this parameter being set!"
	elif sharedStringsIsNotSet "$2"; then
		sharedErrorsThrowError "$BASH_SOURCE" "$FUNCNAME" $LINENO "The internal 'FUNCNAME' parameter has not been passed to this function! The error message cannot be compiled without this parameter being set!"
	elif sharedStringsIsNotSet "$3"; then
		sharedErrorsThrowError "$BASH_SOURCE" "$FUNCNAME" $LINENO "The internal 'LINENO' parameter has not been passed to this function! The error message cannot be compiled without this parameter being set!"
	elif sharedStringsIsNotSet "$4"; then
		sharedErrorsThrowError "$BASH_SOURCE" "$FUNCNAME" $LINENO "The actual message string parameter has not been passed to this function! The error message cannot be compiled without this parameter being set!"
	fi
}

## Outputs warning messages without exit code. Output set to the terminal is colored blue. Output sent to a log is prefixed with 'WARNING: '
## USAGE: sharedErrorsThrowWarning "$BASH_SOURCE" "$FUNCNAME" $LINENO "$message" [$exit_code]
function sharedErrorsThrowWarning {
	if sharedVariablesIsSet "$1" && sharedVariablesIsSet "$2" && sharedVariablesIsSet "$3" && sharedVariablesIsSet "$4"; then
		local source=$1
		local function=$2
		local line_number=$3
		local message=$4
		if sharedVariablesIsSet $5; then
			local exit_code=$5
		else
			local exit_code=1
		fi
		sharedErrorsOutputMessage 5 warning "$source" $function $line_number "$message" $exit_code
	elif sharedStringsIsNotSet "$1"; then
		sharedErrorsThrowError "$BASH_SOURCE" "$FUNCNAME" $LINENO "The internal 'BASH_SOURCE' parameter has not been passed to this function! The error message cannot be compiled without this parameter being set!"
	elif sharedStringsIsNotSet "$2"; then
		sharedErrorsThrowError "$BASH_SOURCE" "$FUNCNAME" $LINENO "The internal 'FUNCNAME' parameter has not been passed to this function! The error message cannot be compiled without this parameter being set!"
	elif sharedStringsIsNotSet "$3"; then
		sharedErrorsThrowError "$BASH_SOURCE" "$FUNCNAME" $LINENO "The internal 'LINENO' parameter has not been passed to this function! The error message cannot be compiled without this parameter being set!"
	elif sharedStringsIsNotSet "$4"; then
		sharedErrorsThrowError "$BASH_SOURCE" "$FUNCNAME" $LINENO "The actual message string parameter has not been passed to this function! The error message cannot be compiled without this parameter being set!"
	fi
}

## Outputs notice messages without exit code. Output set to the terminal is colored green. Output sent to a log is prefixed with 'NOTICE: '
## USAGE: sharedErrorsThrowNotice "$BASH_SOURCE" "$FUNCNAME" "$@" "$message"
function sharedErrorsThrowNotice {
	local colon=:
	local space=" "
	local date=$(date +%A,\ %B\ %e\ %Y)
	local time=$(date +%H:%M:%S)
	sharedColorsSwitchTerminalColors green
	echo "$date at $time$colon$spaceSource File = $1 ... Function = $2 ... Message = $4"
	sharedColorsResetTerminalColors
}

## Sets parameters which allow for a more distinct and easily traceable error message.
## USAGE: sharedErrorsSetCurrentParameters "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "$@"
## RETURN: array PARAMETER_ARRAY
function sharedErrorsSetCurrentParameters {
	passed_array=( `echo "$4"` )
	PARAMETER_ARRAY=([BASH_SOURCE]=$1 [FUNCNAME]=$2 [LINENO]=$3 [PARAMETERS]="${passed_array[@]}")
}
