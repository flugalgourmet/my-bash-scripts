#!/bin/bash

## Contains functions which test filesystem paths for various attributes.


## Copies a file.
## USAGE: sharedFilesystemCopyFile "file_from" "$file_to"
function sharedFilesystemCopyFile {
	sharedErrorsSetCurrentParameters "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "$@"
	if sharedFilesystemIsFile "${1}"; then
		if sharedFilesystemIsFile "${2}"; then
			sharedLogOutputWarning "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The file ""${2}"" already exists... Copying was aborted!"
		else
			cp "${1}" "${2}"
			if sharedFilesystemIsFile "${2}"; then
				sharedLogOutputNotice "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "Copied file from ""${1}"" to the ""${2}"" location."
			else
				sharedLogOutputError "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "Could not copy the file from ""${1}"" to ""${2}"" because the copy command has failed with a(n) "$?" exit status!"
			fi
		fi
	else
		sharedLogOutputError "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "Cannot copy the file ""${1}"" because the file does not exist!"
	fi
}

## Recursively counts the number of files in the given directory.
## USAGE: sharedFilesystemCountFilesRecursively "${root_directory}"
## RETURNS: sharedFilesystemCountFilesRecursively
function sharedFilesystemCountFilesRecursively {
	sharedFilesystemCountFilesRecursively=0
	for directory in "${1}"; do
		numberOfFiles=`find "${directory}" -type f | wc -l`
		sharedFilesystemCountFilesRecursively=$(($sharedFilesystemCountFilesRecursively+$numberOfFiles))
	done
}

## Creates an empty file at the given path.
## USAGE: if sharedFilesystemCreateFile "$path_to_file"; then ... fi
function sharedFilesystemCreateFile {
	sharedErrorsSetCurrentParameters "$BASH_SOURCE" "$FUNCNAME" "$@"
	> "${1}"
	if sharedNumbersIsFailure $?; then
		sharedLogOutputError "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The file at the ""${1}"" path has failed creation!" $?
	else
		sharedLogOutputNotice "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The file at the ""${1}"" path was successfully created."
	fi
}

## Checks if the passed file exists.
## USAGE: if sharedFilesystemExistsFile "$location_to_test"; then ... fi
function sharedFilesystemExistsFile {
	sharedErrorsSetCurrentParameters "$BASH_SOURCE" "$FUNCNAME" "$@"
	if [[ $# -eq 1 ]]; then
		if [[ -e "${1}" ]]; then
			return 0
		else
			return 1
		fi
	else
		sharedErrorsThrowError "$BASH_SOURCE" "$FUNCNAME" $LINENO "The passed parameter which is the location to test is not set!"
	fi
}

## Retrieves the filename from a path to a file.
## USAGE: sharedFilesystemGetFilenameFromPath "$location_to_file_filename.xxx". Will return filename.xxx
function sharedFilesystemGetFilenameFromPath {
	sharedErrorsSetCurrentParameters "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "$@"
	if sharedVariablesIsSet "${1}"; then
		FILENAME=${1##*/}
	else
		sharedErrorsThrowError "$BASH_SOURCE" "$FUNCNAME" $LINENO "The passed parameter which is the location to file filename.xxx is not set!"
	fi
}

## Checks if the passed file exists and is a directory.
## USAGE: if sharedFilesystemIsDirectory "$location_to_test"; then ... fi
function sharedFilesystemIsDirectory {
	sharedErrorsSetCurrentParameters "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "$@"
	if sharedVariablesIsSet "${1}"; then
		if [[ -d "${1}" ]]; then
			return 0
		else
			return 1
		fi
	else
		sharedErrorsThrowError "$BASH_SOURCE" "$FUNCNAME" $LINENO "The passed parameter which is the location to test is not set!"
	fi
}

## Checks if the passed file is empty or not.
## WARNING: The file must exist.
## USAGE: if sharedFilesystemIsEmpty "$location_to_test"; then ... fi
function sharedFilesystemIsEmpty {
	sharedErrorsSetCurrentParameters "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "$@"
	if sharedVariablesIsSet "${1}"; then
		if [[ -s "${1}" ]]; then
			return 1
		else
			return 0
		fi
	else
		sharedErrorsThrowError "$BASH_SOURCE" "$FUNCNAME" $LINENO "The passed parameter which is the location to test is not set!"
	fi
}

## Checks if the passed file exists and is a file.
## USAGE: if sharedFilesystemIsFile "$location_to_test"; then ... fi
function sharedFilesystemIsFile {
	sharedErrorsSetCurrentParameters "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "$@"
	if sharedVariablesIsSet "${1}"; then
		if [[ -f "${1}" ]]; then
			return 0
		else
			return 1
		fi
	else
		sharedErrorsThrowError "$BASH_SOURCE" "$FUNCNAME" $LINENO "The passed parameter which is the location to test is not set!"
	fi
}

## Checks if the passed file exists and is a link.
## USAGE: if sharedFilesystemIsLink "$location_to_test"; then ... fi
function sharedFilesystemIsLink {
	sharedErrorsSetCurrentParameters "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "$@"
	if sharedVariablesIsSet "${1}"; then
		if [[ -L "${1}" ]]; then
			return 0
		else
			return 1
		fi
	else
		sharedErrorsThrowError "$BASH_SOURCE" "$FUNCNAME" $LINENO "The passed parameter which is the location to test is not set!"
	fi
}

## Checks if the passed file exists but is a zero-byte file.
## USAGE: if sharedFilesystemIsZeroByteFile "$location_to_test"; then ... fi
function sharedFilesystemIsZeroByteFile {
	sharedErrorsSetCurrentParameters "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "$@"
	if sharedVariablesIsSet "${1}"; then
		if [[ -s "${1}" ]]; then
			return 1
		else
			return 0
		fi
	else
		sharedErrorsThrowError "$BASH_SOURCE" "$FUNCNAME" $LINENO "The passed parameter which is the location to test is not set!"
	fi
}

## Makes a directory if it does not exist.
## USAGE: sharedFilesystemMakeDirectory "$directory_to_create"
## RETURN: null
function sharedFilesystemMakeDirectory {
	sharedErrorsSetCurrentParameters "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "$@"
	if sharedFilesystemIsDirectory "${1}"; then
		sharedLogOutputWarning "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The directory ""${1}"" already exists and cannot be made."
	else
		exec `mkdir --parents "${1}"`
		if sharedFilesystemIsDirectory "${1}"; then
			sharedLogOutputNotice "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "Created the ""${1}"" directory."
		else
			sharedLogOutputError "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "Could not create the directory of ""${1}"" because the mkdir command has failed with a(n) "$?" exit status!"
		fi
	fi
}

## Moves a file.
## USAGE: sharedFilesystemMoveFile "file_from" "$file_to"
function sharedFilesystemMoveFile {
	sharedFilesystemCopyFile "${1}" "${2}"
	sharedFilesystemRemoveFile "${1}"
}

## Parses a filesystem without recursion and packs the filenames that are top-level directories with a leading dot (omitting the path) to an array.
## USAGE: sharedFilesystemParseDirectoryForTopLevelDirectoriesWithLeadingDot "$location_to_recurse".
## RETURN: array sharedFilesystemParseDirectoryForTopLevelDirectoriesWithLeadingDot
function sharedFilesystemParseDirectoryForTopLevelDirectoriesWithLeadingDot {
	sharedErrorsSetCurrentParameters "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "$@"
	if sharedFilesystemIsDirectory "${1}"; then
		sharedFilesystemParseDirectoryForTopLevelDirectoriesWithLeadingDot=()
		cd "${1}"
		shopt -s dotglob nullglob
		for file in **/; do
            if sharedFilesystemIsDirectory "${1}/${file}"; then
                sharedFilesystemParseDirectoryForTopLevelDirectoriesWithLeadingDot+=("${file}")
            fi
		done
		shopt -u dotglob nullglob
	fi
}

## Parses a filesystem without recursion and packs the filenames that are top-level directories without a leading dot (omitting the path) to an array.
## USAGE: sharedFilesystemParseDirectoryForTopLevelDirectoriesWithoutLeadingDot "$location_to_recurse".
## RETURN: array sharedFilesystemParseDirectoryForTopLevelDirectoriesWithoutLeadingDot
function sharedFilesystemParseDirectoryForTopLevelDirectoriesWithoutLeadingDot {
	sharedErrorsSetCurrentParameters "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "$@"
	if sharedFilesystemIsDirectory "${1}"; then
		sharedFilesystemParseDirectoryForTopLevelDirectoriesWithoutLeadingDot=()
		local index=1
		cd "${1}"
		shopt -s nullglob
		for file in **/; do
            if sharedStringsIsNotFullyEqualTo "${1}/" "${1}/${file}"; then
				sharedStringsStripDirectorySeparatorTrailing "${file}" # Returns sharedStringsStripDirectorySeparatorTrailing
                sharedFilesystemParseDirectoryForTopLevelDirectoriesWithoutLeadingDot[$index]=$sharedStringsStripDirectorySeparatorTrailing
                local index=$(($index+1))
            fi
		done
		shopt -u nullglob
	fi
}

## Parses a filesystem without recursion and packs the filenames that are top-level files with a leading dot (omitting the path) to an array.
## USAGE: sharedFilesystemParseDirectoryForTopLevelFilesWithLeadingDot "$location_to_recurse".
## RETURN: array sharedFilesystemParseDirectoryForTopLevelFilesWithLeadingDot
function sharedFilesystemParseDirectoryForTopLevelFilesWithLeadingDot {
	sharedErrorsSetCurrentParameters "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "$@"
	if sharedFilesystemIsDirectory "${1}"; then
		sharedFilesystemParseDirectoryForTopLevelFilesWithLeadingDot=()
		cd "${1}"
		shopt -s dotglob nullglob
		for file in **; do
			if sharedFilesystemIsFile "${1}/${file}"; then
				sharedFilesystemParseDirectoryForTopLevelFilesWithLeadingDot+=("${file}")
			fi
		done
		shopt -u dotglob nullglob
	fi
}

## Parses a filesystem without recursion and packs the filenames that are top-level files without a leading dot (omitting the path) to an array.
## USAGE: sharedFilesystemParseDirectoryForTopLevelFilesWithoutLeadingDot "$location_to_recurse".
## RETURN: array sharedFilesystemParseDirectoryForTopLevelFilesWithoutLeadingDot
function sharedFilesystemParseDirectoryForTopLevelFilesWithoutLeadingDot {
	sharedErrorsSetCurrentParameters "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "$@"
	if sharedFilesystemIsDirectory "${1}"; then
		sharedFilesystemParseDirectoryForTopLevelFilesWithoutLeadingDot=()
		cd "${1}"
		shopt -s nullglob
		for file in **; do
			if sharedFilesystemIsFile "${1}/${file}"; then
				sharedFilesystemParseDirectoryForTopLevelFilesWithoutLeadingDot+=("${file}")
			fi
		done
		shopt -u nullglob
	fi
}

## Parses a filesystem without recursion and packs the filenames that are top-level subdirectories without a leading dot (omitting the path) to an array.
## USAGE: sharedFilesystemParseDirectoryForTopLevelSubdirectoriesWithoutLeadingDot "$location_to_recurse".
## RETURN: array sharedFilesystemParseDirectoryForTopLevelSubdirectoriesWithoutLeadingDot
function sharedFilesystemParseDirectoryForTopLevelSubdirectoriesWithoutLeadingDot {
	sharedErrorsSetCurrentParameters "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "$@"
	if sharedFilesystemIsDirectory "${1}"; then
		sharedFilesystemParseDirectoryForTopLevelSubdirectoriesWithoutLeadingDot=()
		cd "${1}"
		shopt -s nullglob
		for file in **/; do
			sharedStringsStripDirectorySeparatorTrailing "${file}" # Returns sharedStringsStripDirectorySeparatorTrailing
			sharedFilesystemParseDirectoryForTopLevelSubdirectoriesWithoutLeadingDot+=("${sharedStringsStripDirectorySeparatorTrailing}")
		done
		shopt -u nullglob
	fi
}

## Recurses a filesystem for directories and files with a leading dot and calls a callback sending the callback the current path.
## USAGE: sharedFilesystemRecurseDirectoryAndExecuteCallbackWithLeadingDot "$location_to_recurse".
## RETURN: null
function sharedFilesystemRecurseDirectoryAndExecuteCallbackWithLeadingDot {
	sharedErrorsSetCurrentParameters "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "$@"
	if sharedFilesystemIsDirectory "${1}"; then
		cd "${1}"
		shopt -s globstar dotglob nullglob
		for file in **; do
			if sharedFilesystemIsFile "${1}/${file}"; then
				performExecForFilesystemRecurseDirectoryAndExecuteCallbackPositive "${1}" "${file}"
			else
				performExecForFilesystemRecurseDirectoryAndExecuteCallbackNegative "${1}" "${file}"
			fi
		done
		shopt -u globstar
	fi
}

## Recurses a filesystem for directories and files without a leading dot and calls a callback sending the callback the current path.
## USAGE: sharedFilesystemRecurseDirectoryAndExecuteCallbackWithoutLeadingDot "$location_to_recurse".
## RETURN: null
function sharedFilesystemRecurseDirectoryAndExecuteCallbackWithoutLeadingDot {
	sharedErrorsSetCurrentParameters "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "$@"
	if sharedFilesystemIsDirectory "${1}"; then
		cd "${1}"
		shopt -s globstar
		for file in **; do
			if sharedFilesystemIsFile "${1}/${file}"; then
				performExecForFilesystemRecurseDirectoryAndExecuteCallbackPositive "${1}" "${file}"
			else
				performExecForFilesystemRecurseDirectoryAndExecuteCallbackNegative "${1}" "${file}"
			fi
		done
		shopt -u globstar
	fi
}

## Recurses a filesystem and packs the directories and filenames without a leading dot (omitting the path) to an array.
## USAGE: sharedFilesystemRecurseDirectoryForAll "$location_to_recurse".
## RETURN: array sharedFilesystemRecurseDirectoryForAll
function sharedFilesystemRecurseDirectoryForAll {
	sharedErrorsSetCurrentParameters "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "$@"
	if sharedFilesystemIsDirectory "${1}"; then
		sharedFilesystemRecurseDirectoryForAll=()
		local index=1
		cd "${1}"
		shopt -s globstar
		for file in **; do
			sharedFilesystemRecurseDirectoryForAll[$index]="${file}"
			local index=$(($index+1))
		done
		shopt -u globstar
	fi
}

## Parses a filesystem with recursion and packs the directories that do have a leading dot (omitting the path) to an array.
## USAGE: sharedFilesystemRecurseDirectoryForDirectoriesWithLeadingDot "$location_to_recurse".
## RETURN: array sharedFilesystemRecurseDirectoryForDirectoriesWithLeadingDot
function sharedFilesystemRecurseDirectoryForDirectoriesWithLeadingDot {
	sharedErrorsSetCurrentParameters "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "$@"
	if sharedFilesystemIsDirectory "${1}"; then
		sharedFilesystemRecurseDirectoryForDirectoriesWithLeadingDot=()
		cd "${1}"
		shopt -s globstar dotglob nullglob
		for file in **/; do
			sharedFilesystemRecurseDirectoryForDirectoriesWithLeadingDot+=("${file}")
		done
		shopt -u globstar dotglob nullglob
	fi
}

## Recurses a filesystem and packs the filenames that are files without a leading dot (omitting the path) to an array.
## USAGE: sharedFilesystemRecurseDirectoryForFiles "$location_to_recurse".
## RETURN: array sharedFilesystemRecurseDirectoryForFiles
function sharedFilesystemRecurseDirectoryForFiles {
	sharedErrorsSetCurrentParameters "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "$@"
	if sharedFilesystemIsDirectory "${1}"; then
		sharedFilesystemRecurseDirectoryForFiles=()
		local index=1
		cd "${1}"
		shopt -s globstar
		for file in **; do
			if sharedFilesystemIsFile "${1}/${file}"; then
				sharedFilesystemRecurseDirectoryForFiles[$index]="${file}"
				local index=$(($index+1))
			fi
		done
		shopt -u globstar
	fi
}

## Recurses a filesystem and packs the filenames that are files with a leading dot (omitting the path) to an array.
## USAGE: sharedFilesystemRecurseDirectoryForFilesWithLeadingDot "$location_to_recurse".
## RETURN: array sharedFilesystemRecurseDirectoryForFilesWithLeadingDot
function sharedFilesystemRecurseDirectoryForFilesWithLeadingDot {
	sharedErrorsSetCurrentParameters "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "$@"
	if sharedFilesystemIsDirectory "${1}"; then
		sharedFilesystemRecurseDirectoryForFilesWithLeadingDot=()
		cd "${1}"
		shopt -s globstar dotglob nullglob
		for file in **; do
			if sharedFilesystemIsFile "${1}/${file}"; then
				sharedFilesystemRecurseDirectoryForFilesWithLeadingDot+=("${file}")
			fi
		done
		shopt -u globstar dotglob nullglob
	fi
}

## Parses a filesystem with recursion and packs the directories and filenames that do not have a leading dot (omitting the path) to an array.
## USAGE: sharedFilesystemRecurseDirectoryForFilesAndDirectoriesWithoutLeadingDot "$location_to_recurse".
## RETURN: array sharedFilesystemRecurseDirectoryForFilesAndDirectoriesWithoutLeadingDot
function sharedFilesystemRecurseDirectoryForFilesAndDirectoriesWithoutLeadingDot {
	sharedErrorsSetCurrentParameters "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "$@"
	if sharedFilesystemIsDirectory "${1}"; then
		sharedFilesystemRecurseDirectoryForFilesAndDirectoriesWithoutLeadingDot=()
		local index=1
		cd "${1}"
		shopt -s globstar
		for file in **; do
			sharedFilesystemRecurseDirectoryForFilesAndDirectoriesWithoutLeadingDot[$index]=$file
			local index=$(($index+1))
		done
		shopt -u globstar
	fi
}

## Recurses a filesystem and packs the filenames without a leading dot that are links (omitting the path) to an array.
## USAGE: sharedFilesystemRecurseDirectoryForLinks "$location_to_recurse".
## RETURN: array sharedFilesystemRecurseDirectoryForLinks
function sharedFilesystemRecurseDirectoryForLinks {
	sharedErrorsSetCurrentParameters "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "$@"
	if sharedFilesystemIsDirectory "${1}"; then
		sharedFilesystemRecurseDirectoryForLinks=()
		local index=1
		cd "${1}"
		shopt -s globstar
		for file in **; do
			if sharedFilesystemIsLink "${1}/${file}"; then
				sharedFilesystemRecurseDirectoryForLinks[$index]="${file}"
				local index=$(($index+1))
			fi
		done
		shopt -u globstar
	fi
}

## Recurses a filesystem and packs the directories without a leading dot (omitting the path) to an array.
## USAGE: sharedFilesystemRecurseDirectoryForSubdirectories "$location_to_recurse".
## RETURN: array sharedFilesystemRecurseDirectoryForSubdirectories
function sharedFilesystemRecurseDirectoryForSubdirectories {
	sharedErrorsSetCurrentParameters "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "$@"
	if sharedFilesystemIsDirectory "${1}"; then
		sharedFilesystemRecurseDirectoryForSubdirectories=()
		local index=1
		cd "${1}"
		shopt -s globstar nullglob
		for file in **/; do
			sharedStringsStripDirectorySeparatorTrailing "${file}" # Returns sharedStringsStripDirectorySeparatorTrailing
			sharedFilesystemRecurseDirectoryForSubdirectories[$index]="${sharedStringsStripDirectorySeparatorTrailing}"
			local index=$(($index+1))
		done
		shopt -u globstar nullglob
	fi
}

## Removes a directory and any associated files.
## USAGE: sharedFilesystemRemoveDirectory "$path_to_remove"
function sharedFilesystemRemoveDirectory {
	sharedErrorsSetCurrentParameters "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "$@"
	if sharedFilesystemIsDirectory "${1}"; then
		rm -Rf "${1}"
	else
		sharedLogOutputError "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "Cannot remove ""${1}"" because it is not a directory!"
	fi
}

## Removes the top-level directory from a given absolute path.
## USAGE: sharedFilesystemRemoveDirectoryTopLevel "${workingDirectory}" "${subdirectory}"
function sharedFilesystemRemoveDirectoryTopLevel {
	sharedErrorsSetCurrentParameters "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "$@"
	if sharedFilesystemIsDirectory "${1}"; then
		baseDirectoryArray=(${2//\// })
		baseDirectory="${baseDirectoryArray[0]}"
		sharedLogOutputNotice "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "Removing the '/${baseDirectory}' subdirectory from the given '${2}' subdirectory in the '${1}' path..."
		rm -Rf "${1}/${baseDirectory}"
	else
		sharedLogOutputError "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "Cannot remove ""${1}/${baseDirectory}"" because it is not a directory!"
	fi
}

## Removes a directory.
## USAGE: sharedFilesystemRemoveDirectory "directory_name"
function sharedFilesystemRemoveDirectory {
	sharedErrorsSetCurrentParameters "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "$@"
	if sharedFilesystemIsDirectory "${1}"; then
		rmdir "${1}"
	else
		sharedLogOutputError "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "Cannot remove the directory ""${1}"" because it is not a directory!"
	fi
}

## Removes a file.
## USAGE: sharedFilesystemRemoveFile "file_name"
function sharedFilesystemRemoveFile {
	sharedErrorsSetCurrentParameters "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "$@"
	if sharedFilesystemIsFile "${1}"; then
		rm "${1}"
	else
		sharedLogOutputError "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "Cannot remove the file ""${1}"" because the file does not exist!"
	fi
}
