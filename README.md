
+ ATTENTION: Dar now works in hard drive mode only launched in manual mode or via cron! Optical disc soon to come. Almost all functions work, create, restore, isolate, list and test, except for diff which lists the differences between two backups as I have never found a use for it.
	+ CREATE: tested on 20190707 full and diff from hard drive.
	+ ISOLATE: tested on 20190707 full and diff from hard drive.
	+ LIST: tested on 20190707 full and diff from hard drive.
	+ RESTORE: tested on 20190707 full and diff from hard drive.
	+ TEST: tested on 20190707 full and diff from hard drive.


+ COMING:
	+ dar: Getting there! Lot's of excellent updates already.
		+ Coding the optical disk usage.
		+ Coding diff listing.
		+ Restoring just a single file from an archive (maybe).


+ INFO:
	+ Bash scripts that I created for my own personal use, such as:
	    + alarms: custom timers or set to a certain value.
	        + count down: allows an alarm to be thrown when a countdown timer is exhausted.
	        + count up: allows an alarm to be thrown when a countup timer is exhausted.
	    + archiving: very nice backup solution.
	        + dar (Disk ARchiver): Still a work in progress. Works somewhat.
        + audio:
            + alerts: used by the timers to specify different alert sounds.
            + concatenation: I use this for my web site to concatenate my sound samples for an audio captcha.
            + volume: use this in your desktop files to control the volume of launched applications. My computer volume is set to a fixed value.
        + autostart: auto start any program.
            + conky: requires a predefined conky configuration.
            + ncidd: requires a predefined ncidd configuration and a usb modem with caller id.
            + pidstat: keep an eye on your computer. Write the activity to a series of files. Requires a predefined configuration.
            + screensaver: launch a screensaver. Also requires a predefined configuration.
            + tvheadend: launch a tvheadend server at runtime to record your favorite shows. Requires a predefined configuration and a video capture card.
            + workrave: launch a program that will help with repetitive stress.
            + xboxdrv: launch a utility to allow you to use your joystick or gamepad. Requires a predefined configuration.
        + backends: 
            + akonadirestarter: restarts akonadi, a kde program.
            + akonadistarter: starts akonadi, a kde program.
            + akonadistopper: stops akonadi, a kde program.
            + openxboxdrv: starts the xboxdrv when you need it. Requires the root password.
            + plasmarestarter: restarts plasma when it goes nuts, a kde program.
        + backup: 
            + important: creates a backup of all of your preconfigured directories. Use with cron.
		+ compression: I mod games so I archive them with 7zip. I compress the profiles, mods, and the game separately.
		+ converters:
			+ filename.to.lowercase: Recursively rename all files to lowercase. Needed when modding windows games on linux.
			+ wav.to.flac: Recursively reads wav files and converts them to flac. Names the files according to the cddb.log created from the script "cdparanoia.bsh".
		+ dar (disk archiver): Still does not fully work due to new standards changes. And no, I don't work for kde or amarok.
		+ downloaders:
		    + youtube: Allows you to download via youtube and place your videos into the directory of your choice.
		+ encoders, audio and video
        + frontends: Launches a program using a console started from a desktop file.
        + games: 
        + gaming: 
	    + generators: 
	    + graphics: 
	    + hardware: 
	    + images: 
	    + input: 
	    + installers: 
	    + internet: 
		+ launchers: 
		+ math: 2*2=2+2=2^2
		+ monitoring: 
		+ mount: 
		+ multimedia: 
		+ php: 
		+ results: 
		+ ripping:
			+ cdparanoia: Rips cd's using cdparanoia which writes out a cddb log and a result log for each track. Nice!
		+ security: 
		    + iptables: Allows by simple lists, to accept or reject a numeric ip address or a uri. Configuring the lists is simple with prepopulated examples.
		+ shared: 
		+ system: 
		+ testing: 
		+ time: 
		+ timing: 
		+ utility: 
		+ web: 
	+ Easy to use include system so the shared files can be used in your scripts.
	    + source $self/shared/include.bsh
	+ Unit testing. To run a unit test which exists, run the testing script:
	    + [prefix]/shared/unit_testing -t runUnitTestSharedNumbersIsEqualTo

+ REPOSITORY: I uploaded this repository in hopes that:
	+ People will find it useful.
	+ People will contribute to it.
	+ People will improve it.
	+ People will write more scripts to save them time.
	+ People will combine them with cron, such as for backup, security, compression, etc....
