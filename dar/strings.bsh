#!/bin/bash

## Builds the file path to the log file using the passed parameters.
## USAGE: darStringsBuildLogFilePath "$output_directory" $uppercase_archive_name $uppercase_archive_type timestamp $LOG_EXTENSION $READ_SOURCE
## RETURN: string LOG_FILE
function darStringsBuildLogFilePath {
	dot=.
	separator=/
	underscore=_
	local log_prefix="$1$separator$2$underscore$3$underscore$4"
	LOG_FILE="$log_prefix$dot$6$5"
}

## Takes the parameter passed by BACKUP or BACKUP_CRON and converts it for use as the CREATED_BACKUP_TYPE.
## USAGE: createdBackupType=$(darStringsConvertCreatedBackupType "$archive_type')
## RETURN: string darStringsConvertCreatedBackupType
function darStringsConvertCreatedBackupType {
	sharedErrorsSetCurrentParameters "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "$@"
	if sharedVariablesIsSet "$1"; then
		# Do not change these values.
		if sharedStringsIsFullyEqualTo "$1" "full" || sharedStringsIsFullyEqualTo "$1" "cron_full"; then
			darStringsConvertCreatedBackupType=FULL
		elif sharedStringsIsFullyEqualTo "$1" "cron_diff"; then
			darStringsConvertCreatedBackupType=DIFF
		else
			darStringsConvertCreatedBackupType=DIFF
		fi
	else
		sharedLogOutputError "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The required parameter of the archive type is not set! In order to retrieve the created backup type of 'DIFF' or 'FULL', this parameter must be defined!"
	fi
}

## Finds whether the filename parameter belongs to a full or differential archive type.
## USAGE: $(darStringsFindArchiveType "$selected_archive")
## RETURN: string darStringsFindArchiveType
function darStringsFindArchiveType {
	sharedErrorsSetCurrentParameters "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "$@"
	if sharedVariablesIsSet "$1"; then
		if sharedStringsIsNotEmpty `expr match "$1" '\(^.*_DIFF_\)'`; then
			darStringsFindArchiveType=DIFF
		elif sharedStringsIsNotEmpty `expr match "$1" '\(^.*_FULL_\)'`; then
			darStringsFindArchiveType=FULL
		elif sharedStringsIsNotEmpty `expr match "$1" '\(^.*_ISOLATE_\)'`; then
			darStringsFindArchiveType=ISOLATE
		fi
	else
		sharedLogOutputError "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The 'selected_archive' parameter is not set! Cannot find the backup type of 'FULL', 'DIFF' or 'ISOLATE' without this parameter!"
	fi
	sharedLogOutputNotice "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The archive type '$darStringsFindArchiveType' was found in the archive selection of '$1'"
}

## Returns the string that is found before the trailing slash. Example: 'Programs/HOME-SMITTY_FULL_20120401-151905' returns 'Programs'.
## ATTENTION: Run a unit test with runUnitTestDarStringsFindMediaPath
## USAGE: darStringsFindMediaPath "string_to_parse"
## RETURN: string darStringsFindMediaPath
function darStringsFindMediaPath {
	darStringsFindMediaPath="${1%/*}"
}

## Returns the string that is found after the trailing slash. Example: 'Programs/HOME-SMITTY_FULL_20120401-151905' returns 'HOME-SMITTY_FULL_20120401-151905'.
## ATTENTION: Run a unit test with runUnitTestDarStringsFindMediaFile
## USAGE: darStringsFindMediaFile "string_to_parse"
## RETURN: string darStringsFindMediaFile
function darStringsFindMediaFile {
	darStringsFindMediaFile="${1##*/}"
}

## Iterates through a list of archives which reside in the archive directory and returns the best archive found in those files.
## USAGE: darStringsRetrieveBestArchive "$FILTERED_ARRAY"
## RETURN: YYYYMMDD-HHMMSS SELECTED_ARCHIVE
function darStringsRetrieveBestArchive {
	sharedErrorsSetCurrentParameters "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "$@"
	if sharedVariablesIsSet "$1"; then
		local latest_timestamp=0
		for filename in "${FILTERED_ARRAY[@]}"; do
			foundTimestamp=$(darTimestampFindTimestamp "$filename")
			if sharedNumbersIsGreaterThan $foundTimestamp $latest_timestamp; then
				local latest_timestamp=$foundTimestamp
				darStringsRetrieveBestArchive="$filename"
			fi
		done
	else
		sharedLogOutputError "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The array which contains the file names to parse for the best archive is not set! In order to retrieve the best selected file name from these strings, they must be defined!"
	fi
}
