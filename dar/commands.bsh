#!/bin/bash


## Builds a string with the predefined compression configuration for a certain backup.
## USAGE: darCommandsCompileCompressionString
## RETURN string darCommandsCompileCompressionString
function darCommandsCompileCompressionString {
	sharedErrorsSetCurrentParameters "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "$@"
	local space=" "
	darConstantsGetDefaultCompressionMinimumSize
	darConstantsGetDefaultCompressionType
	darConstantsGetDefaultCompressionLevel
	if sharedNumbersIsGreaterThan $defaultCompressionLevel 0 && sharedStringsIsNotEmpty $defaultCompressionType && sharedVariablesIsInteger $defaultCompressionMinimumSize; then
		darCommandsCompileCompressionString="--mincompr=$darConstantsGetDefaultCompressionMinimumSize$space--compression=$darConstantsGetDefaultCompressionType:$darConstantsGetDefaultCompressionLevel"
	else
		darCommandsCompileCompressionString=""
	fi
}

## Concatenates the command string with the BEEP flag which causes a beep to be emitted when user interaction is required.
## USAGE: darCommandsConcatenateBeepInteractiveTerminalString "${COMMAND}"
## RETURN: string COMMAND
function darCommandsConcatenateBeepInteractiveTerminalString {
	sharedErrorsSetCurrentParameters "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "$@"
	if sharedVariablesIsSet "$1"; then
		local space=" "
		COMMAND="$1$space--beep"
		sharedLogOutputNotice "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The executable command string has been appended to the value of "${COMMAND}""
	else
		sharedLogOutputError "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The 'COMMAND' parameter is not set! The function call for beeping the terminal when user interaction is required cannot be concatenated to the given command string and returned without this parameter being set!"
	fi
}

## Concatenates the command string with the predefined compression configuration for a certain backup.
## USAGE: darCommandsConcatenateCompressionIncludeExcludeString "${COMMAND}" "EXCLUDE_COMPRESSION_ARRAY" "INCLUDE_COMPRESSION_ARRAY"
## RETURN: string COMMAND
function darCommandsConcatenateCompressionIncludeExcludeString {
	sharedErrorsSetCurrentParameters "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "$@"
	if sharedVariablesIsSet "$1" && sharedVariablesIsSet "$2" && sharedVariablesIsSet "$3"; then
		local space=" "
		darCommandsCompileCompressionString ## Returns darCommandsCompileCompressionString
		if sharedStringsIsNotEmpty $darCommandsCompileCompressionString; then
			COMMAND="$1$space$darCommandsCompileCompressionString"
			for exclusion in "${EXCLUDE_COMPRESSION_ARRAY[@]}"; do
				local parameter="--exclude-compression"
				COMMAND="$COMMAND$space$parameter$space$exclusion"
			done
			for inclusion in "${INCLUDE_COMPRESSION_ARRAY[@]}"; do
				local parameter="--include-compression"
				COMMAND="$COMMAND$space$parameter$space$inclusion"
			done
			sharedLogOutputNotice "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The executable command string has been appended to the value of '$COMMAND'"
		fi
	elif sharedStringsIsNotSet "$1"; then
		sharedLogOutputError "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The 'COMMAND' parameter is not set! The command string cannot be concatenated without this parameter being set!"
	elif sharedArraysIsNotSet "$2"; then
		sharedLogOutputError "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The 'EXCLUDE_COMPRESSION_ARRAY' parameter is not set! The command string cannot be concatenated with the compression exclude array without this parameter being set!"
	elif sharedArraysIsNotSet "$3"; then
		sharedLogOutputError "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The 'INCLUDE_COMPRESSION_ARRAY' parameter is not set! The command string cannot be concatenated with the compression include array without this parameter being set!"
	fi
}

## Concatenates the command string with the parameter used in the creation of a new backup.
## USAGE: darCommandsConcatenateCreateNewBackupString "${COMMAND}" "output_file"
## RETURN: string COMMAND
function darCommandsConcatenateCreateNewBackupString {
	sharedErrorsSetCurrentParameters "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "$@"
	if sharedVariablesIsSet "$1" && sharedVariablesIsSet "$2"; then
		local space=" "
		sharedStringsEscapeContainingSpaces "$2" ## Returns sharedStringsEscapeContainingSpaces
		COMMAND="$1$space--create$space$sharedStringsEscapeContainingSpaces"
		sharedLogOutputNotice "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The executable command string has been appended to the value of '$COMMAND'"
	elif sharedStringsIsNotSet "$1"; then
		sharedLogOutputError "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The 'COMMAND' parameter is not set! The command string cannot be concatenated without this parameter being set!"
	elif sharedStringsIsNotSet "$2"; then
		sharedLogOutputError "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "Either the 'COMMAND' parameter or the 'output_file' is not set! The command string cannot be concatenated with the create new backup string without these parameters being set!"
	fi
}

## Concatenates the command string with the parameter used in the difference testing of an existing backup.
## USAGE: darCommandsConcatenateDiffExistingBackupString "${COMMAND}" "$selected_archive"
## RETURN: string COMMAND
function darCommandsConcatenateDiffExistingBackupString {
	sharedErrorsSetCurrentParameters "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "$@"
	if sharedVariablesIsSet "$1" && sharedVariablesIsSet "$2"; then
		local space=" "
		sharedStringsEscapeContainingSpaces "$2" ## Returns sharedStringsEscapeContainingSpaces
		COMMAND="$1$space--diff$space$sharedStringsEscapeContainingSpaces"
		sharedLogOutputNotice "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The executable command string has been appended to the value of '$COMMAND'"
	elif sharedStringsIsNotSet "$1"; then
		sharedLogOutputError "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The 'COMMAND' parameter is not set! The command string cannot be concatenated without this parameter being set!"
	elif sharedStringsIsNotSet "$2"; then
		sharedLogOutputError "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The 'selected_archive' parameter is not set! The command string cannot be concatenated with the difference testing string without this parameter being set!"
	fi
}

## Concatenates the command string with the information required to make a differential backup.
## USAGE: darCommandsConcatenateDiffFilenameString "${COMMAND}" "$archive_directory" "$selected_archive" $archive_type
## RETURN: string COMMAND
function darCommandsConcatenateDiffFilenameString {
	sharedErrorsSetCurrentParameters "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "$@"
	if sharedVariablesIsSet "$1" && sharedVariablesIsSet "$2" && sharedVariablesIsSet "$3" && sharedVariablesIsSet "$4"; then
		archiveDirectory="${2}"
		selectedArchive="${3}"
		archiveType="${4}"
		local space=" "
		local separator=/
		if sharedStringsIsNotFullyEqualTo "$archiveType" "full" && sharedStringsIsNotFullyEqualTo "$archiveType" "cron_full"; then
			if sharedStringsIsFullyEqualTo "$archiveType" "cron_diff"; then
				# Need to retrieve a list of the archives residing in the archive directory
				sharedFilesystemRecurseDirectoryForFiles "$archiveDirectory" # Returns sharedFilesystemRecurseDirectoryForFiles
				darFiltersOnlyIncludeArchives "${sharedFilesystemRecurseDirectoryForFiles[@]}" # Returns darFiltersOnlyIncludeArchives
				# Need to retrieve the best file to run a differential backup on.
				darTimestampSelectArchiveByLatestTimestamp "${darFiltersOnlyIncludeArchives[@]}" # Returns darTimestampSelectArchiveByLatestTimestamp
				local diff_file="$archiveDirectory$separator$darTimestampSelectArchiveByLatestTimestamp"
			else
				# The third parameter is the selected archive.
				local diff_file="$archiveDirectory$separator$selectedArchive"
			fi
			sharedStringsEscapeContainingSpaces "$diff_file" ## Returns sharedStringsEscapeContainingSpaces
			sharedLogOutputNotice "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "$diff_file...$sharedStringsEscapeContainingSpaces"
			COMMAND="$1$space--ref=$sharedStringsEscapeContainingSpaces"
			sharedLogOutputNotice "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The executable command string has been appended to the value of "${COMMAND}""
		else
			sharedLogOutputError "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "An attempt was made to use this function `darCommandsConcatenateDiffFilenameString` when the archive type is `full` or `cron_full`. Please repair."
		fi
	elif sharedStringsIsNotSet "$1"; then
		sharedLogOutputError "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The 'COMMAND' parameter is not set! The command string cannot be concatenated without this parameter being set!"
	elif sharedStringsIsNotSet "$2"; then
		sharedLogOutputError "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The 'archive_directory' parameter is not set! The command string cannot be concatenated with the diff backup string without this parameter being set!"
	elif sharedStringsIsNotSet "$3"; then
		sharedLogOutputError "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The 'selected_archive' parameter (if a diff backup) or the 'archive_type' if a full backup is not set! The command string cannot be concatenated with the difference testing string without this parameter being set!"
	elif sharedStringsIsNotSet "$4"; then
		sharedLogOutputError "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The 'archive_type' parameter is not set! The command string cannot be concatenated with the difference testing string without this parameter being set!"
	fi
}

## Concatenates the command string with the predefined exclude directory configuration for a certain backup.
## USAGE:: darCommandsConcatenateDirectoryExcludeString "${COMMAND}" "$EXCLUDE_DIRECTORY_ARRAY"
## RETURN: string COMMAND
function darCommandsConcatenateDirectoryExcludeString {
	sharedErrorsSetCurrentParameters "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "$@"
	if sharedVariablesIsSet "$1" && sharedVariablesIsSet "$2"; then
		local space=" "
		local substring=""
		for exclusion in "${EXCLUDE_DIRECTORY_ARRAY[@]}"; do
			sharedStringsEscapeContainingSpaces "$exclusion" ## Returns sharedStringsEscapeContainingSpaces
			substring="$substring$space--prune$space"$sharedStringsEscapeContainingSpaces""
		done
		COMMAND="$1$substring"
		sharedLogOutputNotice "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The executable command string has been appended to the value of "${COMMAND}""
	elif sharedStringsIsNotSet "$1"; then
		sharedLogOutputError "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The 'COMMAND' parameter is not set! The command string cannot be concatenated without this parameter being set!"
	elif sharedArraysIsNotSet "$2"; then
		sharedLogOutputError "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The 'EXCLUDE_DIRECTORY_ARRAY' parameter is not set! The command string cannot be concatenated with the directory exclude array without this parameter being set!"
	fi
}

## Concatenates the command string with the program executable.
## USAGE: darCommandsConcatenateExecutableString
## RETURN: string COMMAND
function darCommandsConcatenateExecutableString {
	sharedErrorsSetCurrentParameters "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "$@"
	COMMAND="dar"
	sharedLogOutputNotice "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The executable command string has been appended to the value of "${COMMAND}""
}

## Concatenates the command string with the predefined files and directories to include for a certain backup.
## USAGE: darCommandsConcatenateFilesAndDirectoriesIncludeString "${COMMAND}" "$INCLUDE_FILES_DIRECTORIES_ARRAY" 
## RETURN: string COMMAND
function darCommandsConcatenateFilesAndDirectoriesIncludeString {
	sharedErrorsSetCurrentParameters "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "$@"
	if sharedVariablesIsSet "$1" && sharedVariablesIsSet "$2"; then
		local space=" "
		COMMAND="$1"
		for inclusion in "${INCLUDE_FILES_DIRECTORIES_ARRAY[@]}"; do
			local parameter="--go-into"
			COMMAND="$COMMAND$space$parameter$space$inclusion"
		done
		sharedLogOutputNotice "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The executable command string has been appended to the value of "${COMMAND}""
	elif sharedStringsIsNotSet "$1"; then
		sharedLogOutputError "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The 'COMMAND' parameter is not set! The command string cannot be concatenated without this parameter being set!"
	elif sharedArraysIsNotSet "$2"; then
		sharedLogOutputError "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The 'INCLUDE_FILES_DIRECTORIES_ARRAY' parameter is not set! The command string cannot be concatenated with the files and directories include array without this parameter being set!"
	fi
}

## Concatenates the command string with the predefined included/excluded files configuration for a certain backup.
## USAGE: darCommandsConcatenateFilesIncludeExcludeString "${COMMAND}" "EXCLUDE_FILES_ARRAY" "INCLUDE_FILES_ARRAY"
## RETURN: string COMMAND
function darCommandsConcatenateFilesIncludeExcludeString {
	sharedErrorsSetCurrentParameters "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "$@"
	if sharedVariablesIsSet "$1" && sharedVariablesIsSet "$2" && sharedVariablesIsSet "$3"; then
		local space=" "
		COMMAND="$1"
		for exclusion in "${EXCLUDE_FILES_ARRAY[@]}"; do
			local parameter="--exclude"
			COMMAND="$COMMAND$space$parameter$space$exclusion"
		done
		for inclusion in "${INCLUDE_FILES_ARRAY[@]}"; do
			local parameter="--include"
			COMMAND="$COMMAND$space$parameter$space$inclusion"
		done
		sharedLogOutputNotice "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The executable command string has been appended to the value of "${COMMAND}""
	elif sharedStringsIsNotSet "$1"; then
		sharedLogOutputError "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The 'COMMAND' parameter is not set! The command string cannot be concatenated without this parameter being set!"
	elif sharedArraysIsNotSet "$2"; then
		sharedLogOutputError "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The 'EXCLUDE_FILES_ARRAY' parameter is not set! The command string cannot be concatenated with the files exclude array without this parameter being set!"
	elif sharedArraysIsNotSet "$3"; then
		sharedLogOutputError "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The 'INCLUDE_FILES_ARRAY' parameter is not set! The command string cannot be concatenated with the files include array without this parameter being set!"
	fi
}

## Concatenates the command string with the ignore extended attributes flag which will prevent files with extended attributes from being omitted from restoration.
## USAGE: darCommandsConcatenateIgnoreExtendedAttributesString
## RETURN: string COMMAND
function darCommandsConcatenateIgnoreExtendedAttributesString {
	sharedErrorsSetCurrentParameters "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "$@"
	local space=" "
	COMMAND="$1$space--exclude-ea '*'"
}

## Concatenates the command string with the ignore owner flag which will prevent a warning from being emitted during a restore operation related to the uid being different.
## USAGE: darCommandsConcatenateIgnoreOwnerString
## RETURN: string COMMAND
function darCommandsConcatenateIgnoreOwnerString {
	sharedErrorsSetCurrentParameters "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "$@"
	local space=" "
	COMMAND="$1$space--comparison-field=ignore-owner"
}

## Concatenates the command string with the parameter used in the listing of an existing backup.
## USAGE: darCommandsConcatenateIsolateExistingBackupString "${COMMAND}" "$selected_archive"
## RETURN: string COMMAND
function darCommandsConcatenateIsolateExistingBackupString {
	sharedErrorsSetCurrentParameters "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "$@"
	if sharedVariablesIsSet "$1" && sharedVariablesIsSet "$2"; then
		local space=" "
		sharedStringsEscapeContainingSpaces "$2" ## Returns sharedStringsEscapeContainingSpaces
		COMMAND="$1$space--isolate$space$sharedStringsEscapeContainingSpaces"
		sharedLogOutputNotice "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The executable command string has been appended to the value of "${COMMAND}""
	elif sharedStringsIsNotSet "$1"; then
		sharedLogOutputError "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The 'COMMAND' parameter is not set! The command string cannot be concatenated without this parameter being set!"
	elif sharedStringsIsNotSet "$2"; then
		sharedLogOutputError "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The 'selected_archive' is not set! The command string cannot be concatenated with the isolate existing archive string without this parameter being set!"
	fi
}

## Concatenates the command string with the parameter used in the listing of an existing backup.
## USAGE: darCommandsConcatenateListExistingBackupString "${COMMAND}" "$selected_archive"
## RETURN: string COMMAND
function darCommandsConcatenateListExistingBackupString {
	sharedErrorsSetCurrentParameters "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "$@"
	if sharedVariablesIsSet "$1" && sharedVariablesIsSet "$2"; then
		local space=" "
		sharedStringsEscapeContainingSpaces "$2" ## Returns sharedStringsEscapeContainingSpaces
		COMMAND="$1$space--list$space$sharedStringsEscapeContainingSpaces"
		sharedLogOutputNotice "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The executable command string has been appended to the value of "${COMMAND}""
	elif sharedStringsIsNotSet "$1"; then
		sharedLogOutputError "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The 'COMMAND' parameter is not set! The command string cannot be concatenated without this parameter being set!"
	elif sharedStringsIsNotSet "$2"; then
		sharedLogOutputError "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The 'selected_archive' is not set! The command string cannot be concatenated with the isolate existing archive string without this parameter being set!"
	fi
}

## Concatenates the command string with the VERBOSE-SKIPPED flag.
## USAGE: darCommandsConcatenateLogVerboseSkippedString "${COMMAND}"
## RETURN: string COMMAND
function darCommandsConcatenateLogVerboseSkippedString {
	sharedErrorsSetCurrentParameters "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "$@"
	if sharedVariablesIsSet "$1"; then
		local space=" "
		if darConstantsIsDefaultLogVerboseSkipped; then
			COMMAND="$1$space--verbose=skipped"
			sharedLogOutputNotice "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The executable command string has been appended to the value of "${COMMAND}""
		fi
	else
		sharedLogOutputError "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The 'COMMAND' parameter is not set! The command string cannot be concatenated without this parameter being set!"
	fi
}

## Concatenates the command string with the VERBOSE flag.
## USAGE: darCommandsConcatenateLogVerboseString "${COMMAND}"
## RETURN: string COMMAND
function darCommandsConcatenateLogVerboseString {
	sharedErrorsSetCurrentParameters "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "$@"
	if sharedVariablesIsSet "$1"; then
		local space=" "
		if darConstantsIsDefaultLogVerbose; then
			COMMAND="$1$space--verbose"
			sharedLogOutputNotice "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The executable command string has been appended to the value of "${COMMAND}""
		fi
	else
		sharedLogOutputError "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The 'COMMAND' parameter is not set! The function call for verbose logging cannot be concatenated to the given command string and returned without this parameter being set!"
	fi
}

## Concatenates the command string with the OVERWRITE flag which will prevent an older file from overwriting a newer file.
## USAGE: darCommandsConcatenateOverwriteString
## RETURN: string COMMAND
function darCommandsConcatenateOverwriteString {
	sharedErrorsSetCurrentParameters "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "$@"
	local space=" "
	COMMAND="$1$space--recent"
}

## Concatenates the command string with a flag which will prevent a warning message from being emitted when user interaction is required.
## USAGE: darCommandsConcatenatePreventWarningString "${COMMAND}" "$archive_type"
## RETURN: string COMMAND
function darCommandsConcatenatePreventWarningString {
	sharedErrorsSetCurrentParameters "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "$@"
	if sharedVariablesIsSet "$1" && sharedVariablesIsSet "$2"; then
		local space=" "
		if sharedStringsIsFullyEqualTo "$2" "cron_diff" || sharedStringsIsFullyEqualTo "$2" "cron_full"; then
			COMMAND="$1$space--no-warn"
			sharedLogOutputNotice "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The executable command string has been appended to the value of "${COMMAND}""
		fi
	elif sharedStringsIsNotSet "$1"; then
		sharedLogOutputError "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The 'COMMAND' parameter is not set! The command string cannot be concatenated without this parameter being set!"
	elif sharedStringsIsNotSet "$2"; then
		sharedLogOutputError "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The 'archive_type' is not set! The command string cannot be concatenated with the prevent warning string without these parameters being set!"
	fi
}

## Concatenates the command string with the parameter used in the listing of an existing backup.
## USAGE: darCommandsConcatenateReferenceBackupString "${COMMAND}" "$reference_archive"
## RETURN: string COMMAND
function darCommandsConcatenateReferenceBackupString {
	sharedErrorsSetCurrentParameters "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "$@"
	if sharedVariablesIsSet "$1" && sharedVariablesIsSet "$2"; then
		local space=" "
		sharedStringsEscapeContainingSpaces "$2" ## Returns sharedStringsEscapeContainingSpaces
		COMMAND="$1$space--ref$space$sharedStringsEscapeContainingSpaces"
		sharedLogOutputNotice "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The executable command string has been appended to the value of "${COMMAND}""
	elif sharedStringsIsNotSet "$1"; then
		sharedLogOutputError "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The 'COMMAND' parameter is not set! The command string cannot be concatenated without this parameter being set!"
	elif sharedStringsIsNotSet "$2"; then
		sharedLogOutputError "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The 'reference_archive' parameter is not set! The command string cannot be concatenated with the reference archive string without this parameter being set!"
	fi
}

## Concatenates the command string with the parameter used for the restoration of a backup.
## USAGE: darCommandsConcatenateStringRestoreExistingBackupString "${COMMAND}" "$reference_archive"
## RETURN: string COMMAND
function darCommandsConcatenateRestoreExistingBackupString {
	sharedErrorsSetCurrentParameters "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "$@"
	local space=" "
	COMMAND="$1$space--extract$space$2"
}

## Concatenates the command string with the parameter used to specify the root directory for the required input files to operate upon.
## USAGE: darCommandsConcatenateRootFilesystemInputString "${COMMAND}" "$input_directory"
## RETURN: string COMMAND
function darCommandsConcatenateRootFilesystemInputString {
	sharedErrorsSetCurrentParameters "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "$@"
	if sharedVariablesIsSet "$1" && sharedVariablesIsSet "$2"; then
		local space=" "
		COMMAND="$1$space--fs-root$space'$2'"
		sharedLogOutputNotice "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The executable command string has been appended to the value of "${COMMAND}""
	elif sharedStringsIsNotSet "$1"; then
		sharedLogOutputError "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The 'COMMAND' parameter is not set! The command string cannot be concatenated without this parameter being set!"
	elif sharedStringsIsNotSet "$2"; then
		sharedLogOutputError "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The 'input_directory' parameter is not set! The root directory for the files that need to be operated upon cannot be concatenated to the given command string and returned without this parameter being set!"
	fi
}

## Concatenates the command string with the parameter used to specify the root directory the files will be restored to.
## USAGE: darCommandsConcatenateRootFilesystemOutputString "${COMMAND}" "$output_directory"
## RETURN: string COMMAND
function darCommandsConcatenateRootFilesystemOutputString {
	sharedErrorsSetCurrentParameters "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "$@"
	if sharedVariablesIsSet "$1" && sharedVariablesIsSet "$2"; then
		local space=" "
		COMMAND="$1$space--fs-root$space'$2'"
		sharedLogOutputNotice "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The executable command string has been appended to the value of "${COMMAND}""
	elif sharedStringsIsNotSet "$1"; then
		sharedLogOutputError "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The 'COMMAND' parameter is not set! The command string cannot be concatenated without this parameter being set!"
	elif sharedStringsIsNotSet "$2"; then
		sharedLogOutputError "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The 'output_directory' parameter is not set! The root directory for the location the restored files will go cannot be concatenated to the given command string and returned without this parameter being set!"
	fi
}

## Concatenates the size (in megabytes) of the given backup slice onto the end of the command string.
## USAGE: darCommandsConcatenateSliceSizeString "${COMMAND}"
## RETURN: string COMMAND
function darCommandsConcatenateSliceSizeString {
	sharedErrorsSetCurrentParameters "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "$@"
	if sharedVariablesIsSet "$1"; then
		local space=" "
		darConstantsGetDefaultChunkSize
		COMMAND="$1$space--slice=${darConstantsGetDefaultChunkSize}M" 
		sharedLogOutputNotice "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The executable command string has been appended to the value of "${COMMAND}""
	else
		sharedLogOutputError "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The 'COMMAND' parameter is not set! The function call for defining the current slice size cannot be concatenated to the given command string and returned without this parameter being set!"
	fi
}

## Concatenates the command string with the parameter used in the testing of an existing backup.
## USAGE: darCommandsConcatenateTestExistingBackupString "${COMMAND}" "$selected_archive"
## RETURN: string COMMAND
function darCommandsConcatenateTestExistingBackupString {
	sharedErrorsSetCurrentParameters "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "$@"
	if sharedVariablesIsSet "$1" && sharedVariablesIsSet "$2"; then
		local space=" "
		sharedStringsEscapeContainingSpaces "$2" ## Returns sharedStringsEscapeContainingSpaces
		COMMAND="$1$space--test$space$sharedStringsEscapeContainingSpaces"
		sharedLogOutputNotice "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The executable command string has been appended to the value of "${COMMAND}""
	elif sharedStringsIsNotSet "$1"; then
		sharedLogOutputError "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The 'COMMAND' parameter is not set! The command string cannot be concatenated without this parameter being set!"
	elif sharedStringsIsNotSet "$2"; then
		sharedLogOutputError "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "The 'selected_archive' parameter is not set! The command string cannot be concatenated with the test existing archive string without this parameter being set!"
	fi
}

## Concatenates the command string with the WARN-ALL flag which will prevent a warning from being emitted during an overwrite on all files.
## USAGE: darCommandsConcatenateWarnAllString
## RETURN: string COMMAND
function darCommandsConcatenateWarnAllString {
	sharedErrorsSetCurrentParameters "$BASH_SOURCE" "$FUNCNAME" "$LINENO" "$@"
	local space=" "
	COMMAND="$1$space--no-warn=all"
}

